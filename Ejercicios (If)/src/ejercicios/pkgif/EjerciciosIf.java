package ejercicios.pkgif;
import java.util.Scanner;
public class EjerciciosIf {
    
        static void separador(){
        System.out.println("------------------------------------------------------------------------------");
    }
    
         static void info(){
            System.out.println("56618, Leslie Alejandra Colli Pech");
        }
        
        static void calificaciones(){
            Scanner entrada = new Scanner(System.in );

            float calif;
        
        System.out.println("Ingrese su calificación");
        calif= entrada.nextFloat();
        
        if(calif>=7 && calif<=10){
            System.out.println("Calificación aprobatoria");
        }else if(calif<7){
            System.out.println("Calificación no aprobatoria"); 
        }
        
        }
        static void orden(){
            Scanner entrada = new Scanner(System.in );
                int num_a;
                int num_b;
                int num_c;
                
        System.out.println("Ingrese el primer número");
        num_a= entrada.nextInt(); 
        System.out.println("Ingrese el segundo número");
        num_b= entrada.nextInt(); 
        System.out.println("Ingrese el tercer número");
        num_c= entrada.nextInt(); 
       
 if (num_a > num_b && num_a > num_c) {
    if (num_b > num_c) {
    System.out.println("El orden de mayor a menor es:"+num_a+","+num_b+","+num_c);
      
  } else {
      System.out.println("El orden de mayor a menor es:"+num_a+","+num_c+","+num_b);
    
  }
} else if (num_b > num_a && num_b > num_c) {
    if (num_a > num_c) {
    System.out.println("El orden de mayor a menor es:"+num_b+","+num_a+","+num_c);
 
  } else {
    System.out.println("El orden de mayor a menor es:"+num_b+","+num_c+","+num_a);
   
  }
} else if (num_c > num_a && num_c > num_b) {
    if (num_a > num_b) {
    System.out.println("El orden de mayor a menor es:"+num_c+","+num_a+","+num_b);
    
  } else {
    System.out.println("El orden de mayor a menor es:"+num_c+","+num_b+","+num_a);    
  }
}
}
        static void submenu( int opcion){
           
            
        if (opcion == 1){
            separador();
            System.out.println("Determinar si el alumno aprobó: ");
            calificaciones(); 
            separador();
        }
        if (opcion == 2){
            separador();
            System.out.println("Ordenar 3 números de mayor a menor:");
            orden();
            separador();
        }}
        static void menu(){
       
        System.out.println("1.- Dada la calificación aprobatoria de la UAC, determine si el alumno aprobó. ");
        System.out.println("2.- Pedir tres números y mostrarlos ordenados de mayor a menor. ");
        Scanner entrada = new Scanner( System.in );
        System.out.println("Teclee la opción deseada: ");        
        int iOpcion = entrada.nextInt();
        submenu(iOpcion);}
        
        
public static void main(String[] args) {
info();
separador();
menu();


}
}

